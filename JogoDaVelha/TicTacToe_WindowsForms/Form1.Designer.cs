﻿
using System.Drawing;

namespace JogoDaVelha
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnTic9 = new System.Windows.Forms.Button();
            this.btnTic8 = new System.Windows.Forms.Button();
            this.btnTic7 = new System.Windows.Forms.Button();
            this.btnTic6 = new System.Windows.Forms.Button();
            this.btnTic5 = new System.Windows.Forms.Button();
            this.btnTic4 = new System.Windows.Forms.Button();
            this.btnTic3 = new System.Windows.Forms.Button();
            this.btnTic2 = new System.Windows.Forms.Button();
            this.btnTic1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNovoJogo = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblPlacarJogadorO = new System.Windows.Forms.Label();
            this.lblPlacarJogadorX = new System.Windows.Forms.Label();
            this.lblJogadorO = new System.Windows.Forms.Label();
            this.lblJogadorX = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1281, 131);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(105, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1074, 108);
            this.label1.TabIndex = 0;
            this.label1.Text = "Desafio - Argo Solutions";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnTic9);
            this.panel3.Controls.Add(this.btnTic8);
            this.panel3.Controls.Add(this.btnTic7);
            this.panel3.Controls.Add(this.btnTic6);
            this.panel3.Controls.Add(this.btnTic5);
            this.panel3.Controls.Add(this.btnTic4);
            this.panel3.Controls.Add(this.btnTic3);
            this.panel3.Controls.Add(this.btnTic2);
            this.panel3.Controls.Add(this.btnTic1);
            this.panel3.Location = new System.Drawing.Point(3, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(693, 534);
            this.panel3.TabIndex = 1;
            // 
            // btnTic9
            // 
            this.btnTic9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic9.ForeColor = System.Drawing.Color.Black;
            this.btnTic9.Location = new System.Drawing.Point(473, 368);
            this.btnTic9.Name = "btnTic9";
            this.btnTic9.Size = new System.Drawing.Size(200, 150);
            this.btnTic9.TabIndex = 8;
            this.btnTic9.UseVisualStyleBackColor = false;
            this.btnTic9.Click += new System.EventHandler(this.btnTic9_Click);
            // 
            // btnTic8
            // 
            this.btnTic8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic8.ForeColor = System.Drawing.Color.Black;
            this.btnTic8.Location = new System.Drawing.Point(246, 368);
            this.btnTic8.Name = "btnTic8";
            this.btnTic8.Size = new System.Drawing.Size(200, 150);
            this.btnTic8.TabIndex = 7;
            this.btnTic8.UseVisualStyleBackColor = false;
            this.btnTic8.Click += new System.EventHandler(this.btnTic8_Click);
            // 
            // btnTic7
            // 
            this.btnTic7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic7.ForeColor = System.Drawing.Color.Black;
            this.btnTic7.Location = new System.Drawing.Point(19, 368);
            this.btnTic7.Name = "btnTic7";
            this.btnTic7.Size = new System.Drawing.Size(200, 150);
            this.btnTic7.TabIndex = 6;
            this.btnTic7.UseVisualStyleBackColor = false;
            this.btnTic7.Click += new System.EventHandler(this.btnTic7_Click);
            // 
            // btnTic6
            // 
            this.btnTic6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic6.ForeColor = System.Drawing.Color.Black;
            this.btnTic6.Location = new System.Drawing.Point(473, 192);
            this.btnTic6.Name = "btnTic6";
            this.btnTic6.Size = new System.Drawing.Size(200, 150);
            this.btnTic6.TabIndex = 5;
            this.btnTic6.UseVisualStyleBackColor = false;
            this.btnTic6.Click += new System.EventHandler(this.btnTic6_Click);
            // 
            // btnTic5
            // 
            this.btnTic5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic5.ForeColor = System.Drawing.Color.Black;
            this.btnTic5.Location = new System.Drawing.Point(246, 192);
            this.btnTic5.Name = "btnTic5";
            this.btnTic5.Size = new System.Drawing.Size(200, 150);
            this.btnTic5.TabIndex = 4;
            this.btnTic5.UseVisualStyleBackColor = false;
            this.btnTic5.Click += new System.EventHandler(this.btnTic5_Click);
            // 
            // btnTic4
            // 
            this.btnTic4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic4.ForeColor = System.Drawing.Color.Black;
            this.btnTic4.Location = new System.Drawing.Point(19, 192);
            this.btnTic4.Name = "btnTic4";
            this.btnTic4.Size = new System.Drawing.Size(200, 150);
            this.btnTic4.TabIndex = 3;
            this.btnTic4.UseVisualStyleBackColor = false;
            this.btnTic4.Click += new System.EventHandler(this.btnTic4_Click);
            // 
            // btnTic3
            // 
            this.btnTic3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic3.ForeColor = System.Drawing.Color.Black;
            this.btnTic3.Location = new System.Drawing.Point(473, 14);
            this.btnTic3.Name = "btnTic3";
            this.btnTic3.Size = new System.Drawing.Size(200, 150);
            this.btnTic3.TabIndex = 2;
            this.btnTic3.UseVisualStyleBackColor = false;
            this.btnTic3.Click += new System.EventHandler(this.btnTic3_Click);
            // 
            // btnTic2
            // 
            this.btnTic2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic2.ForeColor = System.Drawing.Color.Black;
            this.btnTic2.Location = new System.Drawing.Point(246, 14);
            this.btnTic2.Name = "btnTic2";
            this.btnTic2.Size = new System.Drawing.Size(200, 150);
            this.btnTic2.TabIndex = 1;
            this.btnTic2.UseVisualStyleBackColor = false;
            this.btnTic2.Click += new System.EventHandler(this.btnTic2_Click);
            // 
            // btnTic1
            // 
            this.btnTic1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTic1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTic1.ForeColor = System.Drawing.Color.Black;
            this.btnTic1.Location = new System.Drawing.Point(19, 14);
            this.btnTic1.Name = "btnTic1";
            this.btnTic1.Size = new System.Drawing.Size(200, 150);
            this.btnTic1.TabIndex = 0;
            this.btnTic1.UseVisualStyleBackColor = false;
            this.btnTic1.Click += new System.EventHandler(this.btnTic1_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(705, 7);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(565, 534);
            this.panel4.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnNovoJogo);
            this.panel6.Controls.Add(this.btnSair);
            this.panel6.Controls.Add(this.btnReiniciar);
            this.panel6.Location = new System.Drawing.Point(16, 274);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(537, 244);
            this.panel6.TabIndex = 2;
            // 
            // btnNovoJogo
            // 
            this.btnNovoJogo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnNovoJogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovoJogo.Location = new System.Drawing.Point(16, 12);
            this.btnNovoJogo.Name = "btnNovoJogo";
            this.btnNovoJogo.Size = new System.Drawing.Size(501, 99);
            this.btnNovoJogo.TabIndex = 2;
            this.btnNovoJogo.Text = "Novo Jogo";
            this.btnNovoJogo.UseVisualStyleBackColor = false;
            this.btnNovoJogo.Click += new System.EventHandler(this.btnNovoJogo_Click);
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Location = new System.Drawing.Point(275, 138);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(242, 99);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnReiniciar.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReiniciar.Location = new System.Drawing.Point(16, 138);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(242, 99);
            this.btnReiniciar.TabIndex = 0;
            this.btnReiniciar.Text = "Reiniciar";
            this.btnReiniciar.UseVisualStyleBackColor = false;
            this.btnReiniciar.Click += new System.EventHandler(this.btnReiniciar_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblPlacarJogadorO);
            this.panel5.Controls.Add(this.lblPlacarJogadorX);
            this.panel5.Controls.Add(this.lblJogadorO);
            this.panel5.Controls.Add(this.lblJogadorX);
            this.panel5.Location = new System.Drawing.Point(16, 14);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(537, 244);
            this.panel5.TabIndex = 1;
            // 
            // lblPlacarJogadorO
            // 
            this.lblPlacarJogadorO.AutoSize = true;
            this.lblPlacarJogadorO.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlacarJogadorO.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblPlacarJogadorO.Location = new System.Drawing.Point(378, 144);
            this.lblPlacarJogadorO.Name = "lblPlacarJogadorO";
            this.lblPlacarJogadorO.Size = new System.Drawing.Size(59, 64);
            this.lblPlacarJogadorO.TabIndex = 4;
            this.lblPlacarJogadorO.Text = "0";
            this.lblPlacarJogadorO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlacarJogadorX
            // 
            this.lblPlacarJogadorX.AutoSize = true;
            this.lblPlacarJogadorX.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlacarJogadorX.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblPlacarJogadorX.Location = new System.Drawing.Point(378, 32);
            this.lblPlacarJogadorX.Name = "lblPlacarJogadorX";
            this.lblPlacarJogadorX.Size = new System.Drawing.Size(59, 64);
            this.lblPlacarJogadorX.TabIndex = 3;
            this.lblPlacarJogadorX.Text = "0";
            this.lblPlacarJogadorX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJogadorO
            // 
            this.lblJogadorO.AutoSize = true;
            this.lblJogadorO.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJogadorO.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblJogadorO.Location = new System.Drawing.Point(10, 144);
            this.lblJogadorO.Name = "lblJogadorO";
            this.lblJogadorO.Size = new System.Drawing.Size(305, 64);
            this.lblJogadorO.TabIndex = 2;
            this.lblJogadorO.Text = "Jogador O:";
            // 
            // lblJogadorX
            // 
            this.lblJogadorX.AutoSize = true;
            this.lblJogadorX.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJogadorX.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblJogadorX.Location = new System.Drawing.Point(10, 32);
            this.lblJogadorX.Name = "lblJogadorX";
            this.lblJogadorX.Size = new System.Drawing.Size(298, 64);
            this.lblJogadorX.TabIndex = 1;
            this.lblJogadorX.Text = "Jogador X:";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(12, 149);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1281, 550);
            this.panel2.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(40)))), ((int)(((byte)(133)))));
            this.ClientSize = new System.Drawing.Size(1346, 712);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jogo da Velha";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnTic9;
        private System.Windows.Forms.Button btnTic8;
        private System.Windows.Forms.Button btnTic7;
        private System.Windows.Forms.Button btnTic6;
        private System.Windows.Forms.Button btnTic5;
        private System.Windows.Forms.Button btnTic4;
        private System.Windows.Forms.Button btnTic3;
        private System.Windows.Forms.Button btnTic2;
        private System.Windows.Forms.Button btnTic1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnNovoJogo;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnReiniciar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblPlacarJogadorO;
        private System.Windows.Forms.Label lblPlacarJogadorX;
        private System.Windows.Forms.Label lblJogadorO;
        private System.Windows.Forms.Label lblJogadorX;
        private System.Windows.Forms.Panel panel2;
    }
}

