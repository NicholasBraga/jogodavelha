﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JogoDaVelha
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Array que representa o tabuleiro do jogo contendo 9 posições (3 linhas e 3 colunas)
        /// </summary>
        String[] tabuleiroDeJogo = new string[9];

        /// <summary>
        /// Variável que representa a jogada atual.
        /// </summary>
        int turnoAtual = 0;

        Color corVerde = Color.FromArgb(114, 198, 162);
        Color corRoxo = Color.FromArgb(105, 40, 133);

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Retorna qual é o símbolo que representa o jogador em função do parâmetro recebido, altera
        /// a cor do jogador da rodada atual para verde e do que está aguardando para roxo.
        /// Se o valor do parâmetro for par, retorna o símbolo "O".
        /// Se o valor do parâmetro for ímpar, retorna o símbolo "X".
        /// </summary>
        /// <param name="turno">Representa o número do turno</param>
        /// <returns></returns>
        protected string RetornaSimboloJogador(int turno)
        {
            if (turno % 2 == 0)
            {
                lblJogadorX.BackColor = corVerde;
                lblJogadorO.BackColor = corRoxo;
                return "O";
            }
            else
            {
                lblJogadorO.BackColor = corVerde;
                lblJogadorX.BackColor = corRoxo;
                return "X";
            }
        }

        /// <summary>
        /// Lógica usada para alterar o estado de todos os botões do tabuleiro para habilitado ou desabilitado de uma só vez.
        /// </summary>
        /// <param name="habilitado">Parâmetro booleano usado para determinar se os botões do tabuleiro estão habilitados ou não.</param>
        protected void interfaceBotoesTabuleiro(Boolean habilitado)
        {
            btnTic1.Enabled = habilitado;
            btnTic2.Enabled = habilitado;
            btnTic3.Enabled = habilitado;
            btnTic4.Enabled = habilitado;
            btnTic5.Enabled = habilitado;
            btnTic6.Enabled = habilitado;
            btnTic7.Enabled = habilitado;
            btnTic8.Enabled = habilitado;
            btnTic9.Enabled = habilitado;
        }

        /// <summary>
        /// Contém a lógica que determina se houve vitória ou empate. 
        /// O jogo possui 8 jogadas que levam a vitória desde que o mesmo símbolo tenha preenchido de forma contígua:
        /// 1 - a 1ª linha
        /// 2 - a 2ª linha
        /// 3 - a 3ª linha
        /// 4 - a 1ª coluna
        /// 5 - a 2ª coluna
        /// 6 - a 3ª coluna
        /// 7 - a diagonal da esquerda para a direita
        /// 8 - a diagonal da direita para a esquerda
        /// O empate acontece quando o jogo chegar até a rodada 9 sem nenhuma jogada de vitória.
        /// A cada jogada, ou seja, a cada clique em um botão que é feito por um jogador, essa função irá percorrer
        /// todo o tabuleiro e verificará se uma das jogadas que levam a vitória foi realizada.
        /// </summary>
        protected void verificaVencedor()
        {
            for (int i = 0; i < 8; i++)
            {
                String combinacao = "";

                switch (i)
                {
                    case 0:
                        combinacao = tabuleiroDeJogo[0] + tabuleiroDeJogo[4] + tabuleiroDeJogo[8];
                        break;
                    case 1:
                        combinacao = tabuleiroDeJogo[2] + tabuleiroDeJogo[4] + tabuleiroDeJogo[6];
                        break;
                    case 2:
                        combinacao = tabuleiroDeJogo[0] + tabuleiroDeJogo[1] + tabuleiroDeJogo[2];
                        break;
                    case 3:
                        combinacao = tabuleiroDeJogo[3] + tabuleiroDeJogo[4] + tabuleiroDeJogo[5];
                        break;
                    case 4:
                        combinacao = tabuleiroDeJogo[6] + tabuleiroDeJogo[7] + tabuleiroDeJogo[8];
                        break;
                    case 5:
                        combinacao = tabuleiroDeJogo[0] + tabuleiroDeJogo[3] + tabuleiroDeJogo[6];
                        break;
                    case 6:
                        combinacao = tabuleiroDeJogo[1] + tabuleiroDeJogo[4] + tabuleiroDeJogo[7];
                        break;
                    case 7:
                        combinacao = tabuleiroDeJogo[2] + tabuleiroDeJogo[5] + tabuleiroDeJogo[8];
                        break;
                }

                if (combinacao.Equals("XXX"))
                {
                    interfaceBotoesTabuleiro(false);
                    lblPlacarJogadorX.Text = (int.Parse(lblPlacarJogadorX.Text) + 1).ToString();
                    MessageBox.Show($"O jogador 1 (X) venceu! \n\n Placar:\n Jogador X = {lblPlacarJogadorX.Text}\n Jogador O = {lblPlacarJogadorO.Text}", "Desafio - Argo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (combinacao.Equals("OOO"))
                {
                    interfaceBotoesTabuleiro(false);
                    lblPlacarJogadorO.Text = (int.Parse(lblPlacarJogadorO.Text) + 1).ToString();
                    MessageBox.Show($"O jogador 2 (O) venceu! \n\n Placar:\n Jogador X = {lblPlacarJogadorX.Text}\n Jogador O = {lblPlacarJogadorO.Text}", "Desafio - Argo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            // Se o jogo chegou até o turno 9 sem que houvessem vencedores, significa dizer que deu empate, ou seja, velha.
            if (turnoAtual == 9)
            {
                interfaceBotoesTabuleiro(false);
                MessageBox.Show("Deu velha!", "Desafio - Argo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnTic1_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[0] = RetornaSimboloJogador(turnoAtual);
            btnTic1.Text = tabuleiroDeJogo[0];
            verificaVencedor();
            btnTic1.Enabled = false;
        }

        private void btnTic2_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[1] = RetornaSimboloJogador(turnoAtual);
            btnTic2.Text = tabuleiroDeJogo[1];
            verificaVencedor();
            btnTic2.Enabled = false;
        }

        private void btnTic3_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[2] = RetornaSimboloJogador(turnoAtual);
            btnTic3.Text = tabuleiroDeJogo[2];
            verificaVencedor();
            btnTic3.Enabled = false;
        }

        private void btnTic4_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[3] = RetornaSimboloJogador(turnoAtual);
            btnTic4.Text = tabuleiroDeJogo[3];
            verificaVencedor();
            btnTic4.Enabled = false;
        }

        private void btnTic5_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[4] = RetornaSimboloJogador(turnoAtual);
            btnTic5.Text = tabuleiroDeJogo[4];
            verificaVencedor();
            btnTic5.Enabled = false;
        }

        private void btnTic6_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[5] = RetornaSimboloJogador(turnoAtual);
            btnTic6.Text = tabuleiroDeJogo[5];
            verificaVencedor();
            btnTic6.Enabled = false;
        }

        private void btnTic7_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[6] = RetornaSimboloJogador(turnoAtual);
            btnTic7.Text = tabuleiroDeJogo[6];
            verificaVencedor();
            btnTic7.Enabled = false;
        }

        private void btnTic8_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[7] = RetornaSimboloJogador(turnoAtual);
            btnTic8.Text = tabuleiroDeJogo[7];
            verificaVencedor();
            btnTic8.Enabled = false;
        }

        private void btnTic9_Click(object sender, EventArgs e)
        {
            turnoAtual++;
            tabuleiroDeJogo[8] = RetornaSimboloJogador(turnoAtual);
            btnTic9.Text = tabuleiroDeJogo[8];
            verificaVencedor();
            btnTic9.Enabled = false;
        }

        /// <summary>
        /// Reinicia os componentes do tabuleiro para que uma nova rodada seja iniciada.
        /// </summary>
        protected void reiniciarTabuleiro()
        {
            turnoAtual = 0;
            tabuleiroDeJogo = new string[9];
            lblJogadorX.BackColor = Color.FromArgb(105, 40, 133);
            lblJogadorO.BackColor = Color.FromArgb(105, 40, 133);

            interfaceBotoesTabuleiro(true);

            btnTic1.Text = "";
            btnTic2.Text = "";
            btnTic3.Text = "";
            btnTic4.Text = "";
            btnTic5.Text = "";
            btnTic6.Text = "";
            btnTic7.Text = "";
            btnTic8.Text = "";
            btnTic9.Text = "";

            btnNovoJogo.Enabled = true;

            btnTic1.BackColor = Color.WhiteSmoke;
            btnTic2.BackColor = Color.WhiteSmoke;
            btnTic3.BackColor = Color.WhiteSmoke;
            btnTic4.BackColor = Color.WhiteSmoke;
            btnTic5.BackColor = Color.WhiteSmoke;
            btnTic6.BackColor = Color.WhiteSmoke;
            btnTic7.BackColor = Color.WhiteSmoke;
            btnTic8.BackColor = Color.WhiteSmoke;
            btnTic9.BackColor = Color.WhiteSmoke;
        }

        /// <summary>
        /// Inicia um novo jogo. Além de resetar as configurações do tabuleiro,
        /// zera o placar dos jogadores.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNovoJogo_Click(object sender, EventArgs e)
        {
            try
            {
                lblPlacarJogadorX.Text = "0";
                lblPlacarJogadorO.Text = "0";
                reiniciarTabuleiro();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Jogo da Velha", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Reinicia os componentes do tabuleiro para que uma nova rodada seja iniciada.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReiniciar_Click(object sender, EventArgs e)
        {
            try
            {
                reiniciarTabuleiro();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Jogo da Velha", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Exibe caixa de diálogo para o usuário perguntando se confirma que deseja fechar o programa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSair_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult exit;
                exit = MessageBox.Show("Tem certeza que deseja sair?", "Jogo da Velha", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (exit == DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Jogo da Velha", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
