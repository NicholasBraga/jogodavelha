#Documentação do Projeto

- Acessar o diretório "JogoDaVelha\html" e abrir o arquivo "index.html".

#Como executar o jogo

- Executar o arquivo "JogoDaVelha.exe"

#Premissa assumida
- Como durante as entrevistas falamos bastante sobre trabalhar com C# e não havia nenhuma informação relativa
a necessidade do projeto ser executável em qualquer sistema operacional, optei por fazê-lo utilizando
Windows Forms, pois entendi que seria mais viável e mais agradável visualmente do que fazê-lo em console, 
como foi sugerido.